# mpa-focal-amd64-nvidia 

metalab package archive (MPA) repository for ubuntu focal 20.04 amd64 nvidia

Fork of https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo

List of packages: https://sat-mtl.gitlab.io/distribution/mpa-focal-amd64-nvidia/debs/dists/sat-metalab/main/binary-amd64/Packages

## Requirements

This repository depends on NVIDIA CUDA repos from https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html#package-manager-ubuntu-install
```
OS="ubuntu2004" && \
wget https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/cuda-${OS}.pin && \
sudo mv cuda-${OS}.pin /etc/apt/preferences.d/cuda-repository-pin-600 && \
#sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/7fa2af80.pub && \
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/3bf863cc.pub && \
sudo add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/ /" && \
sudo apt-get update
```
## Installation

In a terminal:

```sh
sudo apt install -y coreutils wget && \
wget -qO - https://sat-mtl.gitlab.io/distribution/mpa-focal-amd64-nvidia/sat-metalab-mpa-keyring.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg && \
echo 'deb [ arch=amd64, signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-focal-amd64-nvidia/debs/ sat-metalab main' \
    | sudo tee /etc/apt/sources.list.d/sat-metalab-mpa.list && \
echo 'deb [ arch=amd64, signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-datasets/debs/ sat-metalab main' \
    | sudo tee /etc/apt/sources.list.d/sat-metalab-mpa-datasets.list && \
sudo apt update
```

## Automating new packages

- First package your software for Ubuntu: https://packaging.ubuntu.com
- Add user [sat-metalab-mpa](https://gitlab.com/users/sat-metalab-mpa) as Maintainer to the repository of your software (already also added to this mpa), this is required for using the gitlab API with their access token.
- Add the following CI variable to the repository of your software:
   - key: ACCESS_TOKEN
   - value: get from existing repos, for example: https://gitlab.com/sat-mtl/tools/forks/filterpy/-/settings/ci_cd
- In your software repository, `debian/*` branch, append to `.gitlab-ci.yml`:
```
include:
  - remote: 'https://gitlab.com/sat-mtl/distribution/mpa-focal-amd64-nvidia/-/raw/main/.gitlab-ci-package.yml'
```
- In [updaterepos.sh](updaterepos.sh), append the path with namespace of your software repository to variable `projects`, commit, and open a merge request.

## Creating new MPAs

- Select the closest mpa to your needs: https://gitlab.com/sat-mtl/distribution
- Create a new project in subgroup https://gitlab.com/sat-mtl/distribution
  - Choose the **Import project** method
  - Choose **Repo by URL**
  - Use for **Git repository URL**: the HTTPS path of the closest mpa your selected, for example: https://gitlab.com/sat-mtl/distribution/mpa-focal-amd64-nvidia
  - Use for **Project slug**: a name following this pattern: `mpa-${MPA_DISTRO}-${MPA_ARCH}-${MPA_GPU}`
    - Find an example in [.gitlab-ci-package.yml](.gitlab-ci-package.yml) where this pattern defines variable `MPA_NAME`) 
    - Note: `${MPA_GPU}` may alternatively describe the specificity of your mpa in one word
  - Use for **Visibility**: public
- Update each file in separate and granular commits (to facilitate git cherry-picking across mpas):
  - [README.md](README.md)
  - [.gitlab-ci-package.yml](.gitlab-ci-package.yml)
  - [updaterepos.sh](updaterepos.sh)
- Add user [sat-metalab-mpa](https://gitlab.com/users/sat-metalab-mpa) as Maintainer to the repository of your new mpa, this is required for using the gitlab API with their access token.
